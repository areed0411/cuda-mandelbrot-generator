#pragma once
#include <stdint.h>
#include <stdlib.h>

#include "Complex64.h"
class Mandelbrot {
private:

	Complex64 center;
	int width, height;
	double zoom;
	Complex64* cVals;


public:
	Mandelbrot();
	Mandelbrot(int width, int height);
	int IteratePixel(Complex64 c, int iterations);
	void GenerateCVals();
	Complex64* GetCVals() { return cVals; }
	int GetHeight() { return height; }
	int GetWidth() { return width; }
	double GetZoom() { return zoom; }
	void SetZoom(double zoom) { this->zoom = zoom; }
	void SetCenter(Complex64 center) { this->center = center; }
};