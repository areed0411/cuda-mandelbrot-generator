#pragma once

#include <iostream>
using namespace std;

class Complex64
{

public:
	double real, imag;
	Complex64();
	Complex64(double real, double imag);
	void Print();
	double Magnitude();
	Complex64 operator+ (Complex64& other) {
		return Complex64(real + other.real, imag + other.imag);
	}

	Complex64 operator- (Complex64& other) {
		return Complex64(real - other.real, imag - other.imag);
	}

	Complex64 operator* (Complex64& other) {
		return Complex64(real * other.real - imag * other.imag, real * other.imag + imag * other.real);
	}

	Complex64 operator/ (Complex64& other) {
		double divider = pow(other.real, 2) + pow(other.imag, 2);
		return Complex64((real * other.real + imag * other.imag) / divider, (imag * other.real - real * other.imag) / divider);
	}

};

