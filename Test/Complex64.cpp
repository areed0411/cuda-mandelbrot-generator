#include "Complex64.h"


Complex64::Complex64() {
	this->imag = 0;
	this->real = 0;
}

Complex64::Complex64(double real, double imag) {
	this->real = real;
	this->imag = imag;
}

void Complex64::Print() {
	cout << real << " + " << imag << "j";
}

double Complex64::Magnitude()
{
	return sqrt(pow(real, 2) + pow(imag, 2));
}