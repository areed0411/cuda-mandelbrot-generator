#define _CRT_SECURE_NO_WARNINGS
#include "cuda_runtime.h"
#include "device_launch_parameters.h"

#include <iostream>
#include "Mandelbrot.h"
#include "Complex64.h"

#include <opencv2\highgui\highgui.hpp>
#include <opencv2\core\core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <CL/cl.h>
#include <ctime>
using namespace cv;
using namespace std;

#define HEIGHT 1080
#define WIDTH 1920
#define ITERATIONS 5000

#define START_FRAME 0
#define END_FRAME 2
#define ZOOM_RATE 0.999


//Iterate though each complex value. These complex values correspond to the x,y corridate system of the screen that was previously setup by GenerateCVals() in the Mandelbrot class.
__global__
void ParallelGenerate(uint8_t* imgArray, Complex64* result, Complex64* cVals, int iterations) {
	int i = blockIdx.x * blockDim.x + threadIdx.x;
	result[i].real = 0;
	result[i].imag = 0;
	imgArray[i] = 0;
	for (int count = 0; count < iterations; count++) {
		double currReal = result[i].real;
		double currImag = result[i].imag;
		result[i].real = (currReal*currReal - currImag * currImag) + cVals[i].real;
		result[i].imag = (currReal * currImag + currReal * currImag) + cVals[i].imag;
		double magnitude = sqrt(pow(result[i].real, 2) + pow(result[i].imag, 2));
		if (magnitude >= 2) {
			imgArray[i] = (uint8_t)count * 4;
			break;
		}
	}
}
/*	*
	* Attempted to modify the array on the gpu by multiplying all the values by some number. For some reason it just shifted the image upward, rather than zooming it in. 
	* TODO: Fix this. Find a way to do almost everything on the GPU. Apart from the initilizing of the arrays. 
	*/
__global__
void MultiplyArray(Complex64* cVals, double mult) {
	int i = blockIdx.x * blockDim.x + threadIdx.x;
	cVals[i].real = cVals[i].real * mult;
	cVals[i].imag = cVals[i].imag * mult;
}

int main()
{
	int start = 0;
	Mandelbrot test(1000, 1000);
	test.SetZoom(4);
	test.GenerateCVals();
	test.SetCenter(Complex64(0.0016437219711539, 0.8224676332988769));

	int arraySize = test.GetWidth() * test.GetHeight();

	Complex64* cVals = test.GetCVals();
	Complex64* cVals_gpu;
	Complex64* result_gpu;
	uint8_t* imgArray_gpu;
	uint8_t* imgArray = (uint8_t*)malloc(arraySize * sizeof(uint8_t));

	cudaMalloc(&cVals_gpu, arraySize * sizeof(Complex64));
	cudaMalloc(&imgArray_gpu, arraySize * sizeof(uint8_t));
	cudaMalloc(&result_gpu, arraySize * sizeof(Complex64));

	cudaMemcpy(cVals_gpu, cVals, arraySize * sizeof(Complex64), cudaMemcpyHostToDevice);
	
	
	for (int x = START_FRAME; x < END_FRAME; x++) {
		start = clock();
		ParallelGenerate <<<(arraySize + 255) / 256, 256 >>>(imgArray_gpu, result_gpu, cVals_gpu, ITERATIONS);
		cudaMemcpy(imgArray, imgArray_gpu, arraySize * sizeof(uint8_t), cudaMemcpyDeviceToHost);
		
		
		Mat img = Mat(test.GetHeight(), test.GetWidth(), CV_8UC1, imgArray);
		
		//zoom in, regenerate complex values and move onto the gpu.
		test.SetZoom(test.GetZoom()*ZOOM_RATE);
		test.GenerateCVals();
		cudaMemcpy(cVals_gpu, cVals, arraySize * sizeof(Complex64), cudaMemcpyHostToDevice);
		
		//Write the image.
		imwrite("test/img" + to_string(x) + ".jpg", img);
		cout << "Image #" + to_string(x) << ": Completed in " << clock() - start << " mS \n";
	}
	
	//Garbage Cleanup
	cudaFree(cVals_gpu);
	cudaFree(result_gpu);
	cudaFree(imgArray_gpu);
	free(cVals);
	free(imgArray);
	return 0;
}