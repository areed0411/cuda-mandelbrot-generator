#include "Mandelbrot.h"

Mandelbrot::Mandelbrot()
{

	width = 1920;
	height = 1080;

	center = Complex64(0, 0);
	zoom = 1;
	cVals = (Complex64*)malloc(width * height * sizeof(Complex64));

}

Mandelbrot::Mandelbrot(int width, int height)
{

	this->width = width;
	this->height = height;

	center = Complex64(0, 0);
	zoom = 1;
	cVals = (Complex64*)malloc(width * height * sizeof(Complex64));

}

int Mandelbrot::IteratePixel(Complex64 c, int iterations)
{
	Complex64 currVal = c;
	for (int i = 0; i < iterations; i++) {
		Complex64 a(abs(currVal.real), abs(currVal.imag));

		currVal = a * a + c;
		if (currVal.Magnitude() >= 2) {
			return i;
		}
	}
	return 0;
}

//Iterate though each pixel. 
void Mandelbrot::GenerateCVals() {
	for (int x = 0; x < width; x++) {
		for (int y = 0; y < height; y++) {
			Complex64 c((double)zoom * (double)((double)x / (double)width - (double)0.5), ((double)zoom * (double)((double)y / (double)height - (double)0.5)) * ((double)height / (double)width));
			c = c + center;
			//cVals[y,x] = c;
			*(cVals + x + width*y)=Complex64(c.real,c.imag);
		}
	}
}



